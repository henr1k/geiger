/*
 * adc.h
 *
 * Created: 07.03.2016 19.22.51
 *  Author: Henrik
 */ 


#ifndef ADC_H_
#define ADC_H_


uint16_t adc_measure_adc(void);
void adc_init_adc(uint8_t mux, uint8_t prescaler, uint8_t ref, uint8_t left_adjust);

#define ADC_DIV_PRESCALE_FACT_2 0x01
#define ADC_DIV_PRESCALE_FACT_4 0x02
#define ADC_DIV_PRESCALE_FACT_8 0x03
#define ADC_DIV_PRESCALE_FACT_16 0x04
#define ADC_DIV_PRESCALE_FACT_32 0x05
#define ADC_DIV_PRESCALE_FACT_64 0x06
#define ADC_DIV_PRESCALE_FACT_128 0x07

#define ADC_REF_SEL_AREF 0x00
#define ADC_REF_SEL_AVCC 0x01
#define ADC_REF_SEL_INT_1_1 0x03

#define ADC_MUX_SEL_ADC0
#define ADC_MUX_SEL_ADC1 0x01
#define ADC_MUX_SEL_ADC2 0x02
#define ADC_MUX_SEL_ADC3 0x03
#define ADC_MUX_SEL_ADC4 0x04
#define ADC_MUX_SEL_ADC5 0x05
#define ADC_MUX_SEL_ADC6 0x06
#define ADC_MUX_SEL_ADC7 0x07
#define ADC_MUX_SEL_ADC8 0x08
#define ADC_MUX_SEL_1_1V 0x0e
#define ADC_MUX_SEL_0V 0x0f

#define ADC_LEFT_ADJUST 1

#endif /* ADC_H_ */