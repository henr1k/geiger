/*
 * Geiger.c
 *
 * Created: 06.03.2016 16.13.29
 * Author : Henrik
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdio.h>
#include <util/delay.h>
#include "lcd.h"
#include "adc.h"
#include "board.h"
#include "avr_timer.h"
#include "avr_uart.h"
#include "filter.h"
#include "simple_task_manager.h"

volatile uint32_t ms;
static uint32_t tarray[32];
static filter_t tfilter;


void t0_cb(void){
	static uint16_t prescale_second=0;
	
	if(++prescale_second == 20){
		ms++;
		prescale_second = 0;
	}
}

uint32_t get_ms(void){
	return ms;
}

timer_config_t t1_timer = {PRESCALER_DIV8, 49, NULL};
timer_config_t t0_timer = {PRESCALER_DIV8, 49, t0_cb};
	
char lcd_output[32];

volatile uint32_t events = 0;

ISR(INT0_vect){
	static uint32_t last_ms;
	events++;
	filter_put(&tfilter, ms-last_ms);
	RED_LED_ON;
	last_ms = ms;
}


static uint16_t convert_adc_to_volt(uint16_t adc_res){
	uint32_t tmp;
	
	tmp = (adc_res * 10) / 33; //vref
	
	tmp *= 100000;
	
	return (((tmp)/467)/100);
}

static uint32_t get_cpm(void);
static uint8_t update_lcd(struct task_t *task);
static uint8_t update_usb_log(struct task_t *task);


#define TASK_SIZE 4
static task_manager_t geiger_task;
static task_t task_array[TASK_SIZE];

int main(void)
{
	
	filter_init_filter(&tfilter, tarray, 32);
	port_init();
	uart_init(NULL);
	lcd_init(LCD_DISP_ON);
	set_sleep_mode(SLEEP_MODE_IDLE);
	adc_init_adc(ADC_MUX_SEL_ADC7, ADC_DIV_PRESCALE_FACT_64, ADC_REF_SEL_AVCC, 0);
	timer_init_pwm_timer1(&t1_timer, 12, 1);
	timer_init_timer0(&t0_timer);
	
	
	task_init_task_man(&geiger_task, task_array, TASK_SIZE, get_ms);
	task_t update_lcd_task;
	task_t update_usb_log_task;
	
	update_lcd_task.cb = update_lcd;
	update_lcd_task.interval = 50;
	update_lcd_task.priority = S_TASK_MANAGER_PRI_HIGH;
	
	update_usb_log_task.cb = update_usb_log;
	update_usb_log_task.interval = 1000;
	update_usb_log_task.priority = S_TASK_MANAGER_PRI_MED;
	
	
	task_register_new_task(&geiger_task, &update_lcd_task);
	task_register_new_task(&geiger_task, &update_usb_log_task);
	
	printf("starting application...");
	
	while (1){
		task_run_scheduler(&geiger_task);
    }
}

static uint8_t update_lcd(struct task_t *task){
	task_t *t = (task_t*)task;
	t->current_status = TASK_IDLE;
	
	RED_LED_OFF;
	
	lcd_clrscr();
		
	lcd_gotoxy(0,0);
	snprintf(lcd_output,16, "V:%u", convert_adc_to_volt(adc_measure_adc()));
	lcd_puts(lcd_output);
		
	lcd_gotoxy(6,0);
	snprintf(lcd_output, 16, "Evts:%lu", events);
	lcd_puts(lcd_output);
		
	lcd_gotoxy(0,1); //next line
	sprintf(lcd_output, "M:%lu", (ms/1000)/60);
	lcd_puts(lcd_output);
		
	lcd_gotoxy(6,1); //next line
	sprintf(lcd_output, "CPM:%lu", get_cpm());
	lcd_puts(lcd_output);
	
	return 0;
}

static uint8_t update_usb_log(struct task_t *task){
	task_t *t = (task_t*)task;
	t->current_status = TASK_IDLE;
	
	printf("CPM: %lu\n\r", get_cpm());
	
	return 0;
}

static uint32_t get_cpm(void){
	uint32_t time_average = filter_get_avg(&tfilter);
	uint32_t cpm = 0;
		
	if (time_average > 0){
		cpm = ( (60000) / time_average );
	}
	
	return cpm;
}
