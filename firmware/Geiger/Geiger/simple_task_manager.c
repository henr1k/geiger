/*
 * simple_task_manager.c
 *
 *  Created on: 16. nov. 2015
 *      Author: hsk
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <avr/sleep.h>
#include "simple_task_manager.h"


void task_init_task_man(task_manager_t *tm, task_t *task_array, uint8_t task_array_size, task_timer_t t){
	tm->task_array = task_array;
	tm->max_tasks = task_array_size;
	tm->timer = t;

	tm->numof_tasks = 0;
	tm->last_run_ts = 0;

	tm->is_init = 1;
}


static void sort_tasks(task_manager_t *task){
	uint8_t swap_done;
	uint8_t j = 0;
	do{
		swap_done = 0;
		uint8_t n;
		j++;

		for(n=0;n<(task->numof_tasks - j);n++){
			if(task->task_array[n].priority < task->task_array[n+1].priority){
				task_t temp_task;
				memcpy(&temp_task, &task->task_array[n], sizeof(task_t));
				memcpy(&task->task_array[n], &task->task_array[n+1], sizeof(task_t));
				memcpy(&task->task_array[n+1], &temp_task, sizeof(task_t));

				swap_done = 1;
			}
		}
	}while(swap_done);
}




uint8_t task_register_new_task(task_manager_t *task, task_t *new_task){
	uint8_t n;

	if(new_task->priority > S_TASK_MANAGER_PRI_HIGH) return 2;

	for(n=0;n<task->max_tasks;n++){
		task_t *ct = &task->task_array[n];

		if(ct->state == APP_TASK_STATE_FREE){
			ct->cb = new_task->cb;
			ct->interval = new_task->interval;
			ct->priority = new_task->priority;
			ct->last_task_run_ts = 0;
			ct->current_status = TASK_IDLE;
			ct->state = APP_TASK_STATE_USED;

			task->numof_tasks++;

			sort_tasks(task);


			return 0;
		}
	}


	return 1;
}

void task_run_scheduler(task_manager_t *tm){

	if(!tm->is_init) return;


	uint32_t current_ts = tm->timer();

	if(current_ts != tm->last_run_ts){
		tm->last_run_ts = current_ts;

		uint8_t i;
		for(i=0;i<tm->numof_tasks;i++){
			task_t *ctask = &tm->task_array[i];

			uint32_t tdiff = ctask->last_task_run_ts + ctask->interval;

			if(tdiff <= current_ts){
				ctask->cb((struct task_t*)ctask);
				ctask->last_task_run_ts = current_ts;
			}
		}
	} else {
		sleep_mode();
	}
}

uint8_t task_sched_get_busy_tasks(task_manager_t *tm){
	uint8_t n;
	uint8_t busy_tasks = 0;

	for(n=0;n<tm->numof_tasks;n++){
		if(tm->task_array[n].current_status == TASK_BUSY){
			busy_tasks++;
		}
	}

	return busy_tasks;
}


