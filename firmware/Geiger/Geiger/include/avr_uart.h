#ifndef AVR_UART_H_
#define AVR_UART_H_

#include <stdio.h>

//CONFIGURATION
#define FOSC F_CPU
#define BAUD 38400
#define MYUBRR FOSC/16/BAUD-1

typedef void (*uart_isr_handler)(char);

//FUNCTIONS
int uart_putchar(char c, FILE *stream);
void uart_init(uart_isr_handler handler);

#endif