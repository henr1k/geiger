/*
 * board.h
 *
 * Created: 07.03.2016 19.01.52
 *  Author: Henrik
 */ 


#ifndef BOARD_H_
#define BOARD_H_

#define GREEN_LED_ON PORTD |= (1<<4)
#define GREEN_LED_OFF PORTD &= ~(1<<4)

#define RED_LED_ON PORTB |= (1<<0)
#define RED_LED_OFF PORTB &= ~(1<<0)

#define YELLOW_LED_ON PORTB |= (1<<1)
#define YELLOW_LED_OFF PORTB &= ~(1<<1)
void port_init(void);



#endif /* BOARD_H_ */