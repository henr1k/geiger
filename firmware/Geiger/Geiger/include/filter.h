/*
 * filter.h
 *
 * Created: 09.03.2016 21.08.53
 *  Author: Henrik
 */ 


#ifndef FILTER_H_
#define FILTER_H_

typedef struct{
	uint32_t *data;
	uint8_t  mask;
	uint8_t get_ptr;
	uint8_t put_ptr;
	uint8_t elements;
} filter_t;

void filter_init_filter(filter_t *f, uint32_t *buffer, uint8_t size_in_power_of_two);
void filter_put(filter_t *f, uint32_t val);
uint32_t filter_get_avg(filter_t *f);



#endif /* FILTER_H_ */