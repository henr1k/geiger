#include <avr/io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include "avr_uart.h"

static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);

uart_isr_handler this_handler;

ISR(USART_RX_vect){
	this_handler(UDR0);
}

void uart_init(uart_isr_handler handler){
	
	//8bit mode
	UCSR0C = (1<<UCSZ01) | (1<<UCSZ00);
	
	//enable recieve/transmit
	//UCSR0B = (1<<RXCIE0) | (1<<RXEN0) | (1<<TXEN0);
	UCSR0B = (1<<RXCIE0) | (1<<TXEN0);
	
	//load ubbr to register
	UBRR0H = ((MYUBRR) >> 8);
	UBRR0L = (MYUBRR);

	stdout = &mystdout;

	this_handler = handler;
}

int uart_putchar(char c,  FILE *stream){
	if (c == '\n') uart_putchar('\r', stream);
		loop_until_bit_is_set(UCSR0A, UDRE0);
 		UDR0 = c;
	return(1);
}