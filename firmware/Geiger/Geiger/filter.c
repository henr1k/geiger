/*
 * filter.c
 *
 * Created: 09.03.2016 21.06.48
 *  Author: Henrik
 */ 

#include <stdint.h>
#include "filter.h"

void filter_init_filter(filter_t *f, uint32_t *buffer, uint8_t size_in_power_of_two){
	f->data = buffer;
	f->get_ptr = 0;
	f->put_ptr = 0;
	f->mask = size_in_power_of_two-1;
	
	uint8_t i;
	for(i=0;i<size_in_power_of_two;i++)
	f->data[i] = 0;
}

void filter_put(filter_t *f, uint32_t val){
	if(((f->put_ptr - f->get_ptr) & f->mask) == f->mask){		
		f->get_ptr = ((f->get_ptr+1) & f->mask);	
	}	
	
	if(f->elements < (f->mask+1)) f->elements++;
	
	f->data[f->put_ptr] = val;
	f->put_ptr = (f->put_ptr + 1) & f->mask;
}


uint32_t filter_get_avg(filter_t *f){
	uint32_t calc_avg=0;
	uint8_t i;
	uint8_t elements = f->elements;
	
	uint8_t start_ptr = f->get_ptr;
	
	for(i=0;i<elements;i++){
		calc_avg += f->data[start_ptr];
		start_ptr = ((start_ptr+1) & f->mask);
	}
	
	return (calc_avg/elements);
}