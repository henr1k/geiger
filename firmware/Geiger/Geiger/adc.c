/*
 * adc.c
 *
 * Created: 07.03.2016 19.15.51
 *  Author: Henrik
 */ 

#include <avr/io.h>

void adc_stop_adc(void){
	ADMUX = 0x00;
	ADCSRA = 0x00;
}

void adc_init_adc(uint8_t mux, uint8_t prescaler, uint8_t ref, uint8_t left_adjust){
	adc_stop_adc();
	
	ADMUX |= (ref << 6);
	ADMUX |= mux;
	ADMUX |= (left_adjust << 5);
	
	ADCSRA = ((ADCSRA & (~0x07)) | prescaler);
	ADCSRA |= (1<<7); //enable bit 7 (ADCEN)
}

uint16_t adc_measure_adc(void){
	uint8_t n;
	uint32_t adc_res;
	
	//first, get rid of garbage
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));
	
	adc_res = ADC;
	adc_res = 0;
	
	for(n=0;n<4;n++){
		ADCSRA |= (1<<ADSC);
		while(ADCSRA & (1<<ADSC));
		
		adc_res += ADC;
	}
	
	return (uint16_t)(adc_res >> 2);
}