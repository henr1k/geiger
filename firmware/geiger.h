
typedef struct 
{
	uint16_t top;
	uint16_t ocr;
	uint8_t prescaler;

}timer_struct;

#define ADC_DIV_PRESCALE_FACT_2 0x01
#define ADC_DIV_PRESCALE_FACT_4 0x02
#define ADC_DIV_PRESCALE_FACT_8 0x03
#define ADC_DIV_PRESCALE_FACT_16 0x04
#define ADC_DIV_PRESCALE_FACT_32 0x05
#define ADC_DIV_PRESCALE_FACT_64 0x06
#define ADC_DIV_PRESCALE_FACT_128 0x07

#define ADC_REF_SEL_AREF 0x00
#define ADC_REF_SEL_AVCC 0x01
#define ADC_REF_SEL_INT_1_1 0x03

#define ADC_MUX_SEL_ADC0
#define ADC_MUX_SEL_ADC1 0x01
#define ADC_MUX_SEL_ADC2 0x02
#define ADC_MUX_SEL_ADC3 0x03
#define ADC_MUX_SEL_ADC4 0x04
#define ADC_MUX_SEL_ADC5 0x05
#define ADC_MUX_SEL_ADC6 0x06
#define ADC_MUX_SEL_ADC7 0x07
#define ADC_MUX_SEL_ADC8 0x08
#define ADC_MUX_SEL_1_1V 0x0e
#define ADC_MUX_SEL_0V 0x0f

void set_freq_timer1(timer_struct *t);
void set_duty_cycle_timer1(uint16_t dc);
uint16_t get_freq(uint16_t kh_hertz, timer_struct *t);
uint16_t adc_get(void);
void adc_init(void);
uint8_t hv_calibrate(uint16_t volt, timer_struct *t);
void hv_disable(void);
void hv_enable(uint16_t volt, timer_struct *t);