/*
 * simple_task_manager.h
 *
 *  Created on: 16. nov. 2015
 *      Author: hsk
 */

#ifndef SIMPLE_TASK_MANAGER_H_
#define SIMPLE_TASK_MANAGER_H_

struct task_t;
typedef uint8_t (*task_cb_t)(struct task_t *);
typedef uint32_t (*task_timer_t)(void);

enum{
	S_TASK_MANAGER_PRI_LOW,
	S_TASK_MANAGER_PRI_MED,
	S_TASK_MANAGER_PRI_HIGH
};

typedef struct{
	task_cb_t cb;
	uint32_t interval;
	uint32_t last_task_run_ts;
	uint8_t current_status;
	uint8_t priority;
	uint8_t state;
}task_t;

enum{
	TASK_IDLE,
	TASK_BUSY,
	TASK_WAITING
}TASK_STATUS_enum;

typedef struct{
	task_t *task_array;
	uint8_t numof_tasks;

	uint8_t max_tasks;
	task_timer_t timer;

	uint32_t last_run_ts;
	uint8_t is_init;
}task_manager_t;

enum{
	APP_TASK_STATE_FREE,
	APP_TASK_STATE_USED
};

void task_init_task_man(task_manager_t *tm, task_t *task_array, uint8_t task_array_size, task_timer_t t);
uint8_t task_register_new_task(task_manager_t *task, task_t *new_task);
void task_run_scheduler(task_manager_t *tm);
uint8_t task_sched_get_busy_tasks(task_manager_t *tm);


#endif /* SIMPLE_TASK_MANAGER_H_ */