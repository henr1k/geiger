/*
 * board.c
 *
 * Created: 08.03.2016 19.51.16
 *  Author: Henrik
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

void pin_ints(void){
	DDRD &= ~(1<<2); //input
	
	EICRA = (1<<ISC01) | (1<<ISC00);
	EIMSK |= (1<<INT0);
	sei();
}

void port_init(void){
	DDRB |= (1<<2) | (1<<0) | (1<<1);
	DDRD |= (1<<4) | (1<<1);
	

	
	pin_ints();
}