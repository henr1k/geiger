#ifndef AVR_TIMER_H_
#define AVR_TIMER_H_

#define PRESCALER_DISABLE 0x00
#define PRESCALER_DIV1 0x01
#define PRESCALER_DIV8 0x02
#define PRESCALER_DIV64 0x03
#define PRESCALER_DIV256 0x04
#define PRESCALER_DIV1024 0x05


typedef void (*timer0_callback)(void);

typedef struct{
	uint8_t prescaler;
	uint16_t period;
	timer0_callback callback;
}timer_config_t;





void timer_init_timer0(timer_config_t *timer_config);
void timer_init_pwm_timer1(timer_config_t *timer_config, uint8_t wave_gen_mode, uint8_t invert_mode);
void timer1_set_pwm(uint16_t pwm_duty);

#endif