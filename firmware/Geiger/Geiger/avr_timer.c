#include <avr/io.h>
#include <avr/interrupt.h>
#include "avr_timer.h"

timer0_callback current_timer0_callback;



ISR(TIMER0_COMPA_vect){
	current_timer0_callback();
}


void timer_init_timer0(timer_config_t *timer_config){
	current_timer0_callback = timer_config->callback;

	TCCR0A |= (1<<WGM01);
	TIMSK0 |= (1<<OCIE0A);

	OCR0A = timer_config->period;
	TCCR0B |= timer_config->prescaler; //select prescaler
}




void timer_init_pwm_timer1(timer_config_t *timer_config, uint8_t wave_gen_mode, uint8_t invert_mode){
	TCCR1B = (((wave_gen_mode & 0x08)>>3) << 4) | (((wave_gen_mode & 0x04)>>2) << 3);
	TCCR1A = (wave_gen_mode & 0x03);

	TCCR1A |= (0<<COM1B1) | (invert_mode<<COM1B0);

	if(wave_gen_mode == 0x04){
		OCR1B = timer_config->period;
	} else {
		ICR1 = timer_config->period;	
	}


	
	TCCR1B |= timer_config->prescaler;

	//printf("TCCR1B: 0x%.2X, TCCR1A: 0x%.2X, ICR1: %u\r", TCCR1B, TCCR1A, ICR1);
}

void timer1_set_pwm(uint16_t pwm_duty){
	OCR1A = pwm_duty;
}