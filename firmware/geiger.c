/*
geiger v0.1
atmega168
8mhz
*/

//#define F_CPU 8000000UL
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include "geiger.h"
#include "pid.h"

#define FOSC 8000000UL
#define BAUD 38400
#define MYUBRR FOSC/16/BAUD-1

#define LED_ON	PORTB |= (1<<5);
#define LED_OFF PORTB &= ~(1<<5);

void init(void);
void usartInit(void);
static int uart_putchar(char c, FILE *stream);
static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);



uint8_t numof_counts=0;
volatile uint16_t sec=0;
volatile uint8_t event=0;

uint8_t cpm_buffer[60] = {0};
uint8_t idx = 0;

void adc_init_adc(uint8_t mux, uint8_t prescaler, uint8_t ref, uint8_t left_adjust);
uint16_t adc_measure_adc(void);
void adc_stop_adc(void);


ISR(INT0_vect){
	event = 1;

}

ISR(TIMER1_COMPA_vect){
	sec++;
}


int main(void){
	

	init();
	adc_init_adc(ADC_MUX_SEL_ADC5, ADC_DIV_PRESCALE_FACT_64, ADC_REF_SEL_AVCC, 0);
	printf("geiger v0.1\n\r");

	
	pid_t gp;
	pid_init(&gp, 180, 50, 30, 10, 250);
	
	while(1){
		/*
		if(sec != old_sec){
			
			OCR0A = pid_update(&gp, 512, adc_get());
			//pid_print_values(&gp);
			printf("Current PWM: &u\n\r", OCR0A);

			old_sec = sec;
		}
		*/

		LED_ON;
		uint16_t adc_val = adc_measure_adc();
		OCR0A = pid_update(&gp, 580, adc_val);
		LED_OFF;
		_delay_ms(250);

		

	}

}


void timer1_init(void){
	TCCR1B |= (1<<CS12) | (1<<WGM12);
	TIMSK1 |= (1<<OCIE1A);

	OCR1A = 31250;

}

void timer0_init(void){
	TCCR0A |= (1<<COM0A1) | (1<<WGM01) | (1<<WGM00);
	TCCR0B |= (1<<CS01);

	OCR0A = 127;
}


void init(void){
	usartInit();
	PORTC &= ~(1<<5);

	//pin related
	DDRB |= (1<<1) | (1<<5);
	DDRD &= ~(1<<2);
	DDRD |= (1<<6);
	

	EICRA |= (1<<ISC01);
	EIMSK |= (1<<INT0);

	timer1_init();
	timer0_init();
	sei();
}


static int uart_putchar(char c,  FILE *stream){
	if (c == '\n') uart_putchar('\r', stream);
		loop_until_bit_is_set(UCSR0A, UDRE0);
 		UDR0 = c;
	return(1);

}


void usartInit(void)
{
	
	//8bit mode
	UCSR0C |= (1<<UCSZ01) | (1<<UCSZ00);
	
	//enable recieve/transmit
	UCSR0B |= (1<<RXEN0) | (1<<TXEN0);
	
	//load ubbr to register
	UBRR0H = ((MYUBRR) >> 8);
	UBRR0L = (MYUBRR);
	
	
	stdout = &mystdout;
}



void adc_init_adc(uint8_t mux, uint8_t prescaler, uint8_t ref, uint8_t left_adjust){
	adc_stop_adc();
	
	ADMUX |= (ref << 6);
	ADMUX |= mux;
	ADMUX |= (left_adjust << 5);
	
	ADCSRA = ((ADCSRA & (~0x07)) | prescaler);
	ADCSRA |= (1<<7); //enable bit 7 (ADCEN)
}

void adc_stop_adc(void){
	ADMUX = 0x00;
	ADCSRA = 0x00;
}

uint16_t adc_measure_adc(void){
	uint8_t n;
	uint32_t adc_res;
	
	//first, get rid of garbage
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));
	
	adc_res = ADC;
	adc_res = 0;
	
	for(n=0;n<4;n++){
		ADCSRA |= (1<<ADSC);
		while(ADCSRA & (1<<ADSC));
		
		adc_res += ADC;
	}
	
	return (uint16_t)(adc_res >> 2);
}
